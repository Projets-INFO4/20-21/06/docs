# **PROJECT 6 : MOBILE APPLICATION REGISTERING IoT NODES**

- The project code is located in this [Github](https://github.com/CampusIoT/lorawan-device-identification-qr-codes) repository.

- You can find in this repository : 
    - The tracking sheet of the project : [TRACKING_SHEET.md](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/06/docs/-/blob/master/TRACKING_SHEET.md)
    - The final report : [Final_Report.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/06/docs/-/blob/master/Final_Report.pdf)
    - The final presentation : [Final_Presentation.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/06/docs/-/blob/master/Final_Presentation.pdf)
    - The mid-term presentation : [Mid-term_Presentation.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/06/docs/-/blob/master/Mid-term_Presentation.pdf)
    - The tutorial of the application **ScanWAN** : [HowToUse.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/06/docs/-/blob/master/HowToUse.pdf)
    - Integration Tests report : [Integration_Tests.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/06/docs/-/blob/master/Integration_Tests.pdf)
